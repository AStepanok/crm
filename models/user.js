var mongoose = require('mongoose'),
    passportLocalMongoose = require('passport-local-mongoose');

var UserSchema = new mongoose.Schema({
    username: String,
    password: String,
    leads: [{type: mongoose.Schema.Types.ObjectId, ref: 'Lead'}],
    fullName: String,
    email: String,
    telNumber: String,
    img: String,
    colorscheme: String
});

UserSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', UserSchema);