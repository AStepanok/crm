var mongoose = require('mongoose');

var LeadSchema = new mongoose.Schema({
    _user:  {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    fio: String,
    email: String,
    telNumber: String,
    status: String,
    initialComments: String,
    initialDate: String

});



module.exports = mongoose.model('Lead', LeadSchema);