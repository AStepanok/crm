var express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    passport = require('passport'),
    LocalStrategy = require('passport-local'),
    passportLocalMongoose = require('passport-local-mongoose'),
    bodyParser = require('body-parser'),
    multer = require('multer'),
    fs = require('fs'),
    rimraf = require('rimraf'),
    nodemailer = require('nodemailer'),
    User = require('./models/user'),
    Lead = require('./models/lead'),
    Emails = require('./models/emails');

mongoose.connect('mongodb://localhost/crm');

app.use(require('express-session')({
    secret: 'Sample secret words',
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge : 24 * 3600000 } //1 Hour
}));



app.set('view engine','ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use('/public', express.static('public'));
app.use('/eliteadmin/', express.static(__dirname + '/node_modules/eliteadmin/'));
app.use('/uploads/', express.static(__dirname + '/uploads/'));
app.use('/font-awesome/css/', express.static(__dirname + '/node_modules/fontawesome-free-5.0.8/dist/css'));
app.use('/font-awesome/fonts/', express.static(__dirname + '/node_modules/fontawesome-free-5.0.8/dist/webfonts'));

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'email',
        pass: 'password'
    }
});


app.get('/', isLoggedIn, function (req, res) {
    res.redirect('/requests');
    // res.render('index', {
    //     title: 'Обзор - Rocket Websites CRM',
    //     user: req.user
    // });
});

app.get('/profile', isLoggedIn, function (req, res) {
    res.render('profile', {
        title: 'Профиль - Rocket Websites CRM',
        user: req.user
    });
});

app.get('/login', function (req, res) {
    res.render('login');
});

app.get('/requests', isLoggedIn, function (req, res) {
    var _user;
    User.findOne({_id: req.user._id}).populate('leads').exec(function (err, __user) {

        _user = __user;
        res.render('requests', {
            title: 'Заявки - Rocket Websites CRM',
            user: req.user,
            requests_init: _user.leads.filter(x => x.status == "init"),
            requests_talks: _user.leads.filter(x => x.status == "talks"),
            requests_work: _user.leads.filter(x => x.status == "work"),
            requests_done: _user.leads.filter(x => x.status == "done")
        });
    });


});

app.get('/requests/new', isLoggedIn, function (req, res) {
   res.render('new_request', {
       title: 'Новая заявка - Rocket Websites CRM',
       user: req.user
    });
});

app.get('/requests/:id', isLoggedIn, function (req, res) {
    Lead.findOne({_id: req.params.id}, function (err, _req) {
        if (err) {
            console.log(err);
        } else {
            res.render('edit_request', {
                title: 'Изменить заявку - Rocket Websites CRM',
                user: req.user,
                req: _req
            });
        }
    });

});

app.get('/requests/:id/totalks', isLoggedIn, function (req, res) {
    Lead.findOne({_id: req.params.id}, function (err, lead) {
        if (err) {
            console.log(err);
        } else {
            lead.status='talks';
            lead.save(function (err) {
                res.json({
                    success: 'true',
                    code: '200'
                });
            });
        }
    });
});

app.get('/requests/:id/toinit', isLoggedIn, function (req, res) {
    Lead.findOne({_id: req.params.id}, function (err, lead) {
        if (err) {
            console.log(err);
        } else {
            lead.status='init';
            lead.save(function (err) {
                res.json({
                    success: 'true',
                    code: '200'
                });
            });
        }
    });
});

app.get('/requests/:id/towork', isLoggedIn, function (req, res) {
    Lead.findOne({_id: req.params.id}, function (err, lead) {
        if (err) {
            console.log(err);
        } else {
            lead.status='work';
            lead.save(function (err) {
                res.json({
                    success: 'true',
                    code: '200'
                });
            });
        }
    });
});

app.get('/requests/:id/todone', isLoggedIn, function (req, res) {
    Lead.findOne({_id: req.params.id}, function (err, lead) {
        if (err) {
            console.log(err);
        } else {
            lead.status='done';
            lead.save(function (err) {
                res.json({
                    success: 'true',
                    code: '200'
                });
            });
        }
    });
});

app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
});


app.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login'
}) ,function (req, res) {

});

app.post('/profile',isLoggedIn, function (req, res) {
   req.user.fullName = req.body.fullName;
   req.user.email = req.body.email;
   req.user.telNumber = req.body.telNumber;
   req.user.save(function () {
       res.redirect('/profile');
   })
});

app.post('/profile/img',isLoggedIn, function (req, res) {
    var upload = multer({dest: 'uploads/' + req.user._id}).array('file');
    upload(req, res, function (err) {
        if (err)
            console.log(err);
        else {
            req.user.img = '/uploads/'+req.user._id+'/'+req.files[0].filename;

            req.user.save(function () {
                res.redirect('/profile');
            })
        }

    });
});

app.post('/profile/colorsheme',isLoggedIn, function (req, res) {
   req.user.colorscheme = req.body.colorscheme;
   req.user.save();
});

app.post('/requests', function (req, res) {

    User.findOne({_id: req.body._id}, function (err, u) {
        if (err || u === undefined)
            console.log(err);
        else {

            Lead.create({
                _user: u._id,
                fio: req.body.fio,
                email: req.body.email,
                telNumber: req.body.telNumber,
                initialDate: req.body.initialDate,
                initialComments: req.body.initialComments,
                status: 'init'
            }, function (err, lead) {
                if (err)
                    console.log(err);
                else {
                    u.leads.push(lead);
                    u.save(function () {
                        res.redirect('/requests');
                    })
                }
            })
        }

    });

});



app.post('/requests/:id', isLoggedIn, function (req, res) {
   Lead.update({_id: req.params.id}, {
       $set: {
           fio: req.body.fio,
           email: req.body.email,
           telNumber: req.body.telNumber,
           initialDate: req.body.initialDate,
           initialComments: req.body.initialComments
       }
   }, function (err, lead) {
      if (err) {
          console.log(err);
      } else {
          res.redirect('/requests');
      }
   });
});

app.post('/email',function (req, res) {

    var properitesNames = {
        'fio': 'ФИО',
        'telNumber': 'Номер телефона',
        'email': 'Email',
        'initialComments': 'Комментарий',
        'products': 'Продукция',
        'addressAndDate': 'Адрес и время доставки'
    };

    var email = Emails.find(x => x._id == req.body._id);
    if (email !== undefined) {

        var mailOptions = {
            from: 'anton.stepanok@gmail.com',
            to: email.email,
            subject: 'Новое обращение с Вашего Лендинга'
        };
        var bodyStr = "";
        for (var key in req.body.emailData) {
            bodyStr += '<h3>' + properitesNames[key] + ":</h3> <p>" + req.body.emailData[key] + '</p>';
        }
        mailOptions.html = '<h1>Вам пришла новая заявка с лендинга!</h1>' + bodyStr;
        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
                res.end('{"error" : "Something went wrong", "status" : 500}');
            } else {
                console.log('Email sent: ' + info.response);
                res.end('{"success" : "Email sent succesfully", "status" : 200}');
            }
        });
    } else {

        res.end('{"error" : "Wrong token", "status" : 500}');
    }
});

app.delete('/requests/:id', isLoggedIn, function (req, res) {
    console.log('we"re here');
    User.findOne({_id: req.user.id}).populate('leads').exec(function (err, user) {
        user.leads.splice(user.leads.indexOf(user.leads.find(x => x._id == req.params.id)),1);
        user.save(function (err, u) {
            if (err)
                console.log(err);
            else {
                Lead.remove({_id: req.params.id}, function (err) {
                    if (err)
                        console.log(err);
                    else
                        res.json({
                            success: 'true',
                            code: '200'
                        })
                });
            }
        })
    });
});





function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {

        return next();
    }
    res.redirect('/login');
}

app.listen(3000, function () {
    console.log('Server is running');
});